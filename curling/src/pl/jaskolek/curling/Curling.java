package pl.jaskolek.curling;
import pl.jaskolek.curling.screens.MenuScreen;
import pl.jaskolek.curling.ui.BaseScreen;
import pl.jaskolek.curling.ui.UiApp;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

public class Curling extends UiApp {
	
	@Override
	public void create() {
		generateAtlas();
		Assets.init( new TextureAtlas( Gdx.files.internal("data/assets.atlas") ) );
		
		super.create();
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		super.render();
		Table.drawDebug(stage);
	}


	private void generateAtlas(){
		TexturePacker2.process("../curling-android/assets/raw", "../curling-android/assets/data", "assets.atlas");
	}
	
	@Override
	protected String atlasPath() {
		return "data/uiskin.atlas";
	}

	@Override
	protected String skinPath() {
		return "data/uiskin.json";
	}

	@Override
	protected void styleSkin(Skin skin, TextureAtlas atlas) {
		
	}

	@Override
	protected BaseScreen getFirstScreen() {
		return new MenuScreen(this, skin);
	}

}
