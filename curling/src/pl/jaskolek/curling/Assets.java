package pl.jaskolek.curling;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;

public class Assets {

	public static AtlasRegion stoneTexture;
	public static AtlasRegion menuBgTexture;
	public static AtlasRegion iceTexture;
	
	public static void init( TextureAtlas atlas ){
		stoneTexture	= atlas.findRegion("stone");
		menuBgTexture	= atlas.findRegion("menu-bg");
		iceTexture		= atlas.findRegion("ice");
	}
	
}
