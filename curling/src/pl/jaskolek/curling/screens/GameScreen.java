package pl.jaskolek.curling.screens;

import pl.jaskolek.curling.actors.Board;
import pl.jaskolek.curling.ui.BaseScreen;
import pl.jaskolek.curling.ui.UiApp;

public class GameScreen extends BaseScreen {
	Board board;
	
	public GameScreen(UiApp app) {
		super(app);
		
		mainTable.setSize(Board.WIDTH, Board.HEIGHT);
		
		mainTable.debug();
		Board board	= new Board();
		mainTable.add( board ).expand().row();
	}
	
	@Override
	public void onBackPress() {
	}

}
