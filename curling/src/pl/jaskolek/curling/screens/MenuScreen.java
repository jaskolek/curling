package pl.jaskolek.curling.screens;

import pl.jaskolek.curling.Assets;
import pl.jaskolek.curling.ui.BaseScreen;
import pl.jaskolek.curling.ui.UiApp;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MenuScreen extends BaseScreen {
	Skin		skin;
	
	public MenuScreen(UiApp app, Skin skin) {
		super(app);
		this.skin	= skin;
		mainTable.debug();
		
		TextButton startGameButton	= new TextButton("Start game!", skin);
		startGameButton.addCaptureListener( startGameButtonListener );
		
		TextButton optionsButton	= new TextButton("Opcje", skin);
		optionsButton.addCaptureListener(optionsButtonListener);
		
		Label titleLabel	= new Label("Curling! WOLOLOOOOO", skin);
		
		mainTable.setBackground( new TextureRegionDrawable( Assets.menuBgTexture ) );
		mainTable.add( titleLabel ).top().row();
		
		Table buttonsTable	= new Table();
		buttonsTable.add( startGameButton ).width(150).uniform().space(30).row();
		buttonsTable.add( optionsButton ).width(150);
		
		mainTable.row();
		mainTable.add( buttonsTable ).expand();
	}
	
	
	@Override
	public void onBackPress() {}

	
	

	ClickListener startGameButtonListener	= new ClickListener(){
		public void clicked(InputEvent event, float x, float y) {
			app.switchScreens( new GameScreen(app) );
		};
	};
	
	ClickListener optionsButtonListener 	= new ClickListener(){
		public void clicked(InputEvent event, float x, float y) {};
	};
}
