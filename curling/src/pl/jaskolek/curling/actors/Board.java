package pl.jaskolek.curling.actors;

import pl.jaskolek.curling.Assets;
import pl.jaskolek.curling.actors.target.Ring;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;

public class Board extends Group {
	public enum CameraState {
		STONE_TRACK, RETURN, RETURNED
	};

	public static final int WIDTH = 2000;
	public static final int HEIGHT = 720;

	public static final Vector2 TARGET_POSITION = new Vector2(1500, HEIGHT / 2);
	public static final Vector2 START_POSITION = new Vector2(100, HEIGHT / 2);

	public static final float STONE_RADIUS = 32;

	public static final int VIEWPORT_WIDTH = 800;
	public static final int VIEWPORT_HEIGHT = 480;
	public static final float VIEWPORT_MAX_CHANGE = 0.5f;
	public static final float VIEWPORT_MAX_SCALE = 1.5f;
	public static final int VIEWPORT_MIN_SCALE = 1;

	public static final float RETURN_SPEED = 2000;

	private Stone trackedStone;
	private CameraState cameraState = CameraState.STONE_TRACK;

	private float currentViewportScale = 1;

	Array<Stone> stones = new Array<Stone>();

	public Board() {
		setSize(WIDTH, HEIGHT);

		Target t = new Target(TARGET_POSITION);
		addActor(t);

		t.addRing(new Ring(200, 10, Color.BLUE));
		t.addRing(new Ring(150, 25, Color.WHITE));
		t.addRing(new Ring(100, 50, Color.RED));
		t.addRing(new Ring(50, 100, Color.WHITE));

		trackedStone = createStone();
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		AtlasRegion texture = Assets.iceTexture;

		int th = texture.getRegionHeight();
		int tw = texture.getRegionWidth();
		for (int startY = 0; startY < HEIGHT; startY += th) {
			for (int startX = 0; startX < WIDTH; startX += tw) {
				batch.draw(texture, startX, startY);
			}
		}

		super.draw(batch, parentAlpha);
	}

	public Stone createStone() {
		Stone stone = new Stone(START_POSITION, STONE_RADIUS);
		stones.add(stone);
		addActor(stone);

		return stone;
	}

	@Override
	public void act(float delta) {
		// collision detection
		for (int i = 0; i < stones.size; ++i) {
			calculateWallCollision(stones.get(i));
			for (int j = i + 1; j < stones.size; ++j) {
				if (Intersector.overlaps(stones.get(i).getCircle(),
						stones.get(j).getCircle())) {
					calculateStonesCollision(stones.get(i), stones.get(j));
				}
			}
		}

		if (trackedStone != null) {
			// stone has stopped - add points and return camera
			if (trackedStone.getState() == Stone.StoneState.STOPPED) {
				trackedStone = null;
				cameraState = CameraState.RETURN;
			}
		}

		if (cameraState == CameraState.RETURNED) {
			trackedStone = createStone();
			cameraState = CameraState.STONE_TRACK;
		}

		// track stone, or return to starting point
		if (cameraState == CameraState.STONE_TRACK) {
			trackStone(delta);
		} else {
			returnCamera(delta);
		}

		super.act(delta);
	}

	private void returnCamera(float delta) {
		Vector3 cameraPosition = getStage().getCamera().position.cpy();
		
		float maxSpeed = delta * RETURN_SPEED;

		float desiredSpeedX = START_POSITION.x - cameraPosition.x;
		float desiredSpeedY = START_POSITION.y - cameraPosition.y;
		float signumX = Math.signum(desiredSpeedX);
		float signumY = Math.signum(desiredSpeedY);

		float speedX = signumX * Math.min(maxSpeed, Math.abs(desiredSpeedX));
		float speedY = signumY * Math.min(maxSpeed, Math.abs(desiredSpeedY));

		moveCameraTo(new Vector2(cameraPosition.x + speedX, cameraPosition.y
				+ speedY));
		
		if( cameraPosition.equals( getStage().getCamera().position ) ){
			cameraState = CameraState.RETURNED;
		}
	}

	private void trackStone(float delta) {
		// stone tracking //
		float viewportMaxChange = VIEWPORT_MAX_CHANGE * delta;
		float viewportScale = 0.7f + trackedStone.getVelocity().len() / 400;

		// float viewportScale = (trackedStone.getVelocity().len() <
		// 100)?VIEWPORT_MIN_SCALE:VIEWPORT_MAX_SCALE;

		// zooming camera
		viewportScale = Math.min(viewportScale, VIEWPORT_MAX_SCALE);
		viewportScale = Math.max(viewportScale, VIEWPORT_MIN_SCALE);
		if (viewportScale > currentViewportScale) {
			currentViewportScale = Math.min(currentViewportScale
					+ viewportMaxChange, viewportScale);
		} else {
			currentViewportScale = Math.max(currentViewportScale
					- viewportMaxChange, viewportScale);
		}

		getStage().getCamera().viewportWidth = VIEWPORT_WIDTH
				* currentViewportScale;
		getStage().getCamera().viewportHeight = VIEWPORT_HEIGHT
				* currentViewportScale;

		// moving camera
		moveCameraTo(trackedStone.getCenterPosition());

	}

	private void moveCameraTo(Vector2 position) {
		float paddingX = currentViewportScale * VIEWPORT_WIDTH / 2;
		position.x = Math.max(position.x, paddingX);
		position.x = Math.min(position.x, getWidth() - paddingX);

		float paddingY = currentViewportScale * VIEWPORT_HEIGHT / 2;
		position.y = Math.max(position.y, paddingY);
		position.y = Math.min(position.y, getHeight() - paddingY);

		getStage().getCamera().position.set(position, 1);
	}

	private void calculateWallCollision(Stone stone) {
		if (stone.getX() < 0 && stone.getVelocity().x < 0) {
			stone.getVelocity().x *= -1;
		} else if (stone.getX() + 2 * stone.getRadius() > getWidth()
				&& stone.getVelocity().x > 0) {
			stone.getVelocity().x *= -1;
		} else if (stone.getY() < 0 && stone.getVelocity().y < 0) {
			stone.getVelocity().y *= -1;
		} else if (stone.getY() + 2 * stone.getRadius() > getHeight()
				&& stone.getVelocity().y > 0) {
			stone.getVelocity().y *= -1;
		}
	}

	private void calculateStonesCollision(Stone stone1, Stone stone2) {
		// zaczepiamy w srodek stone1
		Vector2 pOffset = new Vector2(stone2.getX() - stone1.getX(),
				stone2.getY() - stone1.getY());

		// roznica predkosci - srodek w stone1
		Vector2 vOffset = stone1.getVelocity().cpy().sub(stone2.getVelocity());

		float angle = pOffset.angle() - vOffset.angle();
		float len = vOffset.len() * MathUtils.cosDeg(angle) * 0.7f;
		if (len < 0)
			return;

		Vector2 diff = new Vector2(len, 0);
		diff.rotate(pOffset.angle());

		stone1.getVelocity().sub(diff);
		stone2.getVelocity().add(diff);

		float bufRot = stone1.getRotVelocity();
		stone1.setRotVelocity(bufRot * 0.7f + stone2.getRotVelocity() * 0.3f);
		stone2.setRotVelocity(bufRot * 0.3f + stone2.getRotVelocity() * 0.7f);
	}

}
