package pl.jaskolek.curling.actors;

import java.util.ArrayList;

import pl.jaskolek.curling.actors.target.Ring;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Target extends Actor {

	ArrayList<Ring> rings = new ArrayList<Ring>();
	ShapeRenderer shapeRenderer = new ShapeRenderer();

	float radius = 0;

	public Target(float x, float y) {
		setPosition(x, y);
	}

	public Target(Vector2 position) {
		this(position.x, position.y);
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.end();

		shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
		shapeRenderer.setTransformMatrix(batch.getTransformMatrix());

		shapeRenderer.begin(ShapeType.Filled);

		for (Ring ring : rings) {
			shapeRenderer.setColor(ring.color);
			shapeRenderer.circle(getX() + radius, getY() + radius, ring.radius);
		}
		shapeRenderer.end();
		batch.begin();
	}

	public void addRing(Ring ring) {
		rings.add(ring);

		float radiusDiff = radius - ring.radius;
		if (radiusDiff < 0) {
			radius = ring.radius;
			translate(radiusDiff, radiusDiff);
		}
	}

	public void setCenterPosition(float x, float y) {
		setPosition(x + radius, y + radius);
	}

	public float getPositionPoints(float x, float y) {
		float xLength = (getX() - x);
		float yLength = (getY() - y);

		float length = (float) Math.sqrt( xLength * xLength + yLength * yLength );

		float points = 0;
		for( Ring ring: rings ){
			if( ring.radius > length ){
				points = ring.points;
			}else{
				break;
			}
		}
		
		return points;
	}
}
