package pl.jaskolek.curling.actors;

import pl.jaskolek.curling.Assets;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

public class Stone extends Actor {
	public enum StoneState {
		BEFORE_MOVE, MOVING, STOPPED
	};

	public StoneState stoneState = StoneState.BEFORE_MOVE;

	private Vector2 velocity = new Vector2(0, 0);
	private float rotVelocity = 0;

	private float friction = 50;
	private float rotFriction = 30;

	private float radius = 32;

	public Stone(float x, float y, float radius) {
		super();
		this.radius = radius;

		setSize(radius * 2, radius * 2);
		setOrigin(radius, radius);
		setPosition(x - radius, y - radius);
		addListener(listener);
	}

	public Stone(Vector2 position, float radius) {
		this(position.x, position.y, radius);
	}

	public Stone(float x, float y, float radius, Touchable touchable) {
		this(x, y, radius);
		setTouchable(touchable);
	}

	@Override
	public Actor hit(float x, float y, boolean touchable) {
		if (getTouchable() == Touchable.disabled) {
			return null;
		}

		if ((x - getOriginX()) * (x - getOriginX()) + (y - getOriginY())
				* (y - getOriginY()) < radius * radius) {
			return this;
		} else {
			return null;
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.draw(Assets.stoneTexture, getX(), getY(), getOriginX(),
				getOriginY(), getWidth(), getHeight(), getScaleX(),
				getScaleY(), getRotation());
	}

	@Override
	public void act(float delta) {

		calculateRotVelocity(delta);
		rotate(rotVelocity * delta);

		calculateVelocity(delta);
		translate(velocity.x * delta, velocity.y * delta);

		super.act(delta);
	}

	private void calculateRotVelocity(float delta) {
		float sign = Math.signum(rotVelocity);
		rotVelocity -= rotFriction * sign * delta;

		if (sign != Math.signum(rotVelocity)) {
			rotVelocity = 0;
		}
	}

	private void calculateVelocity(float delta) {
		if (velocity.x == 0 && velocity.y == 0)
			return;

		// stosunek nowej predkosci do starej
		float diff = 1 - friction * delta / velocity.len();

		if (diff < 0) {
			velocity.set(0, 0);
			if (stoneState == StoneState.MOVING) {
				stoneState = StoneState.STOPPED;
			}
		} else {
			velocity = velocity.scl(diff);
		}
	}

	ActorGestureListener listener = new ActorGestureListener() {
		public void fling(com.badlogic.gdx.scenes.scene2d.InputEvent event,
				float velocityX, float velocityY, int button) {
			setTouchable(Touchable.disabled);
			stoneState = StoneState.MOVING;

			// @TODO dorobic rotacje i wywalic liczby do stalych/opcji
			velocity.set(velocityX / 5, velocityY / 5);
			rotVelocity = 250 - (float) Math.random() * 500;
		}

	};

	public Vector2 getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public float getRotVelocity() {
		return rotVelocity;
	}

	public void setRotVelocity(float rotVelocity) {
		this.rotVelocity = rotVelocity;
	}

	public Circle getCircle() {
		return new Circle(new Vector2(getX(), getY()), radius);
	}

	public Vector2 getPosition() {
		return new Vector2(getX(), getY());
	}

	public Vector2 getCenterPosition() {
		return new Vector2(getX() + getOriginX(), getY() + getOriginY());
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public StoneState getState(){
		return stoneState;
	}
}
