package pl.jaskolek.curling.actors.target;

import com.badlogic.gdx.graphics.Color;


public class Ring {
	public float radius;
	public float points;
	public Color color;

	public Ring(float radius, float points, Color color) {
		super();
		this.radius = radius;
		this.points = points;
		this.color = color;
	}
	
}